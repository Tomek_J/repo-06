package pl.air.bookstore;

import java.util.List;
import java.util.SortedSet;

import pl.air.bookstore.model.Author;
import pl.air.bookstore.model.Book;
import pl.air.bookstore.model.Customer;
import pl.air.bookstore.model.Order;
import pl.air.bookstore.model.OrderLine;
import pl.air.bookstore.model.OrderLineComparator;

public class main {

	public static void main(String[] args) {
		main m = new main();
		m.createDate();
	}

	private void createDate()
	{
		Author fredro = new Author("Aleksander", "Fredro");
		Book zemsta = new Book("Zemsta", 2012);
		Book sluby = new Book("�luby panie�skie", 1984);
		
		zemsta.addAuthor(fredro);
		sluby.addAuthor(fredro);
		
		printBook(zemsta);
		printBook(sluby);
		
		Book uml = new Book("J�zyk UML 2.0 w modelowaniu system�w informatycznych", 2009);
		Author wrycza = new Author("Stanis�aw", "Wrycza");
		Author marcinkowski = new Author("Bogumi�", "Marcinkowski");
		Author wyrzykowski = new Author("Krzysztof", "Wyrzykowski");
		
			uml.addAuthor(wrycza);
			uml.addAuthor(marcinkowski);
			uml.addAuthor(wyrzykowski);
			
			printBook(uml);
			
			Customer ewa = new Customer("Ewa", "Nowak");
			Order ewaOrder = new Order("176.04.04.2017", ewa);
			
			OrderLine line1 = new OrderLine(2, zemsta, 1);
			OrderLine line2 = new OrderLine(1, uml, 10);
			OrderLine line3 = new OrderLine(3, sluby, 5);
			OrderLine line4 = new OrderLine(3,zemsta,7);
			
			ewaOrder.addOrderLine(line1);
			ewaOrder.addOrderLine(line2);
			ewaOrder.addOrderLine(line3);
			ewaOrder.addOrderLine(line4);

			printOrder(ewaOrder);
	}
	
	private void printOrder(Order order){
		System.out.println(order.getOrderNo() + " --> " + order.getCustomer().getLastName());
		
		SortedSet<OrderLine> orderLines = order.getOrderLines();
		for(OrderLine orderLine : orderLines){
		if(OrderLine.change == 1){
		}
			printOrderLine(orderLine);
		}
		
		System.out.println();
	}
	

	private void printOrderLine(OrderLine orderLine){
		System.out.println(orderLine.getSeqNo() + ". " + orderLine.getBook().getTitle() + " - " + orderLine.getQuantity());
	}
	
	private void printBook(Book book){
		System.out.println(book.getTitle() + ", " + book.getPubYear());
		
		/*Author[] authors = book.getAuthors();
		
		for(int i = 0; i < book.getAuthorIdx(); i++){
			Author author = authors[i];
			printAuthor(author);
		}*/
		List<Author> authors = book.getAuthors();
		for(Author author : authors){
			printAuthor(author);
		}
		System.out.println();
	}
	
	private void printAuthor(Author author){
		System.out.println(author.getFirstName() + ", " + author.getLastName());
	}
	
	private static void createDate2()
	{
		
	}
}
