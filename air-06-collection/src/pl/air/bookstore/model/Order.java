package pl.air.bookstore.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.SortedSet;
import java.util.TreeSet;

public class Order {
	
    private String orderNo;
    private LocalDateTime orderDateTime;
    private String status;
    private Customer customer;

    private SortedSet<OrderLine> orderLines = new TreeSet<>(new OrderLineComparator());
    
    public void addOrderLine(OrderLine orderLine){
    	orderLines.add(orderLine);    	
    }
    
    
    public SortedSet<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(SortedSet<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public Order() {
    }
    
    public Order(String orderNo, Customer customer) {
    	this.orderNo = orderNo;
    	this.orderDateTime = LocalDateTime.now();
    	this.status = "CREATED";     // should be refactored to enum   
    	this.customer = customer;
    }

    
    public String getOrderNo() {
        return this.orderNo;
    }
    public void setOrderNo(String value) {
        this.orderNo = value;
    }

    public LocalDateTime getOrderDateTime() {
        return this.orderDateTime;
    }
    public void setOrderDateTime(LocalDateTime value) {
        this.orderDateTime = value;
    }

    public String getStatus() {
        return this.status;
    }
    public void setStatus(String value) {
        this.status = value;
    }

    public Customer getCustomer() {
        return this.customer;
    }
    public void setCustomer(Customer value) {
        this.customer = value;
    }


	@Override
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return orderNo + "  -->  " + orderDateTime.format(formatter);
	}
    
}
