package pl.air.bookstore.model;


public class Publisher {
	
    private String name;

    
    public Publisher() {
    }

    
    public String getName() {
        return this.name;
    }
    public void setName(String value) {
        this.name = value;
    }


	@Override
	public String toString() {
		return name;
	}
    
}
