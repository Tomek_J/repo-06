package pl.air.bookstore.model;


public class Author {
	
    private String firstName;
    private String lastName;

    
    public Author() {
    }
    
    public Author(String firstName, String lastName) {
    	this.firstName = firstName;
    	this.lastName = lastName;
    }


    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String value) {
        this.lastName = value;
    }


	@Override
	public String toString() {
		return firstName + " " + lastName;
	}
    
}
