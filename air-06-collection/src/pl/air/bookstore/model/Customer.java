package pl.air.bookstore.model;

public class Customer {
	
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private Account account;

    
    public Customer() {
    }
    
    public Customer(String firstName, String lastName) {
    	this.firstName = firstName;
    	this.lastName = lastName;
    }

    public Customer(String firstName, String lastName, String username, String password) {
    	this.firstName = firstName;
    	this.lastName = lastName;
    	this.account = new Account(username, password);
    }
    
    
    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String value) {
        this.lastName = value;
    }
    
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String value) {
        this.address = value;
    }

    public String getEmail() {
        return this.email;
    }
    public void setEmail(String value) {
        this.email = value;
    }

    public Account getAccount() {
        return this.account;
    }
    public void setAccount(Account value) {
        this.account = value;
    }


	@Override
	public String toString() {
		return firstName + " " + lastName + (account == null ? "" : ", account: " + account.getUsername());
	}
    
}
