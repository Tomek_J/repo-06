package pl.air.bookstore.model;


public class Account {
	
    private String username;
    private String password;

    
    public Account() {
    }

    
    public Account(String username, String password) {
    	this.username = username;
    	this.password = password;
    }
    
        
    public String getUsername() {
        return this.username;
    }
    public void setUsername(String value) {
        this.username = value;
    }

    public String getPassword() {
        return this.password;
    }
    public void setPassword(String value) {
        this.password = value;
    }


	@Override
	public String toString() {
		return username;
	}
    
}
