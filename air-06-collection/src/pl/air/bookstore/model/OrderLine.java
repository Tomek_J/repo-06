package pl.air.bookstore.model;

public class OrderLine {
	
	public static int change;
    private int seqNo;
    private Book book;
    private int quantity;

    
    public OrderLine() {
    }
    
    public OrderLine(int seqNo, Book book, int quantity) {
    	this.seqNo = seqNo;
    	this.book = book;
    	this.quantity = quantity;
    }

    
  
	public int getSeqNo() {
        return this.seqNo;
    }
    public void setSeqNo(int value) {
        this.seqNo = value;
    }

    public Book getBook() {
        return this.book;
    }
    public void setBook(Book value) {
        this.book = value;
    }

    public int getQuantity() {
        return this.quantity;
    }
    public void setQuantity(int value) {
        this.quantity = value;
    }

}
